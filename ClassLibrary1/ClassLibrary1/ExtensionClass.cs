﻿using System;

namespace ClassLibrary1
{
    public static class ExtensionClass
    {
        public static int Sub(this ClassLibrary1.Calculator obj, int a,int b)
        {
            return a > b ? a - b : b - a;
        }
    }
}

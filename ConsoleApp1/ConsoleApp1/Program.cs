﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static int m1(int n)
        {
            int m2(int n1)
            {
                return (n1 < 2) ? 1 : n1 * m2(n1 - 1);
            }
            return m2(n);
        }
        static void Main(string[] args)
        {
            Console.WriteLine(m1(5));
        }
    }
}

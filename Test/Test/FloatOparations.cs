﻿using System;

namespace Test
{
    
    class FloatOparations
    {
        public static string a, b;
        public static void add(string[] a1,string[] b1)
        {
            int n1 = int.Parse(a1[0]), n2 = int.Parse(a1[1]);
            int m1 = int.Parse(b1[0]), m2 = int.Parse(b1[1]);
            int d = n2 + m2, nd = n1 + m1;
            string res = "";
            if (d.ToString().Length > a1[1].Length)
            {
                res = d.ToString().Substring(1);
                nd++;
            }
            if (d.ToString().Length < a1[1].Length)
            {
                for (int i = d.ToString().Length; i < a1[1].Length; i++)
                {
                    res += "0";
                }
                res += d.ToString();
            }
            else
            {
                res = d.ToString();
            }
            res = nd.ToString() + "." + res;
            Console.WriteLine(res);
        }
        public static void sub(string[] a1,string[] b1)
        {
            
            int n1 = int.Parse(a1[0]), n2 = int.Parse(a1[1]);
            int m1 = int.Parse(b1[0]), m2 = int.Parse(b1[1]);
            int nd = n1 - m1, d = n2 - m2;
            string res = "";
            if (d < 0)
            {
                int ten = 1;
                for(int i=0;i< a1[1].Length; i++)
                {
                    ten *= 10;
                }
                d += ten;
                nd--;
            }
            res = nd.ToString();
            if (d.ToString().Length < a1[1].Length)
            {
                res += ".";
                for(int i = d.ToString().Length; i < a1[1].Length; i++)
                {
                    res += "0";
                }
                res += d.ToString();
            }
            else
            {
                res += "." + d.ToString();
            }
            Console.WriteLine(res);
        }
        public static void Main(string[] args)
        {
            a = Console.ReadLine();
            b=Console.ReadLine();
            string[] a1 = a.Split("."),b1=b.Split(".");
            int l1 = a1[1].Length, l2 = b1[1].Length;
            if(l1<l2)
            {
                for(int i = l1; i < l2; i++)
                {
                    a1[1] +="0";
                }
            }
            else if(l2<l1)
            {
                for (int i = l2; i < l1; i++)
                {
                    b1[1] += "0";
                }
            }
            a = a1[0] + "." + a1[1];
            b = b1[0] + "." + b1[1];
            add(a1, b1);
            sub(a1, b1);
        }
    }
}

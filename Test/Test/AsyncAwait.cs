﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace Test
{
    class AsyncAwait
    {
        
        public static async Task Method1()
        {
            //await Task.Run(() =>
            //{
                for(int i = 0; i < 5; i++)
                {
                    Console.WriteLine("Method1 called");
                    //Task.Delay(4).Wait();
                }
            //}
            //);
        }
        public static  void Method2()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Method2 called");
                Task.Delay(4).Wait();
            }
        }
        public static async Task Main(string[] args)
        {
            await Method1();
            Method2();
            
        }
    }
}

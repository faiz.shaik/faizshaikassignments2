﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class MatrixMult
    {
        public int[,] a;
        public MatrixMult(int[,] m1)
        {
            a = m1;
        }
        public static int[,] operator +(MatrixMult ma,MatrixMult mb)
        {
            int sum = 0;
            int[,] m1 = ma.a;
            int[,] m2 = mb.a;
            int[,] res = new int[m1.GetLength(0),m1.GetLength(1)];
            for(int i = 0; i < m1.GetLength(0); i++)
            {
                for(int j = 0; j < m1.GetLength(1); j++)
                {
                    for (int k = 0; k < m1.GetLength(1); k++)
                    {
                        sum += (m1[i, k] * m2[k,j]);
                    }
                    res[i, j] = sum;
                    sum = 0;
                }
            }
            return res;
        }
        public static int[,] operator -(MatrixMult ma, MatrixMult mb)
        {
            int[,] m1 = ma.a;
            int[,] m2 = mb.a;
            int[,] res = new int[m1.GetLength(0), m1.GetLength(1)];
            for (int i = 0; i < m1.GetLength(0); i++)
            {
                for (int j = 0; j < m1.GetLength(1); j++)
                {
                    res[i, j] = m1[i,j]-m2[i,j];
                    
                }
            }
            return res;
        }
        public static void Main(string[] args)
        {
            /*int[,] m1 = new int[2, 2] { { 2, 3 }, { 4, 5 } };
            int[,] m2 = new int[2, 2] { { 6, 7 }, { 8, 9 } };
            MatrixMult mm1 = new MatrixMult(m1);
            MatrixMult mm2 = new MatrixMult(m2);
            int[,] res= mm1+mm2;
            for(int i = 0; i < res.GetLength(0); i++)
            {
                for(int j = 0; j < res.GetLength(1); j++)
                {
                    Console.Write(res[i,j]+" ");
                }
                Console.WriteLine();
            }
            int[] a = { 1,2,3,4,5};
            int[] b = a[^1..];
            Console.WriteLine(string.Join(" ",b));*/
            //int? a =null;
            /*
            int? x = null;
            int y = 2;
            int? c;
            c = x ??= (1+y);
            Console.WriteLine(c.ToString());*/
            /*int a1 = 0;
            Type t = typeof(Type);
            Type t1 = a1.GetType();
            Console.WriteLine(t);
            Console.WriteLine(t1);
            Console.WriteLine(t1.AssemblyQualifiedName);*/
            /*StringBuilder a = new StringBuilder("abc");
            a[1] = '2';
            Console.WriteLine(a.AppendFormat("1",a));
            string[] a11 = { "1","2", "3", "4", "5" };
            string[] b11 = a11[1..];
            Console.WriteLine(string.Join(" ",b11));*/
            //Console.WriteLine(a11[^111..]);
            string a = "ab";
            string b = "abcde".Substring(0,2);
            string c = b;
            string s=null;
            string s1 = null;
            Console.WriteLine(s==s1);
            Console.WriteLine((a==b)+" "+a.Equals(b));
            Console.WriteLine((a==c)+" "+a.Equals(c));
        }
    }
}

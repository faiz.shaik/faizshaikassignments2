﻿using System;

namespace Test
{
    interface i1
    {
        int m1();
    }
    interface i2
    {
        string m1();
    }
    class c1 : i1,i2
    {
        int i1.m1()
        {
            return 0;
        }
        string i2.m1()
        {
            return "hey";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            c1 c = new c1();
            //Console.WriteLine(c.m1());
            i1 i = new c1();
            Console.WriteLine(i.m1());
            i2 i0 = new c1();
            Console.WriteLine(i0.m1());
            
        }
    }
}

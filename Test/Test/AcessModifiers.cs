﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class AcessModifiers
    {
        private static int b = 100;
        class T
        {
            private static int a = 10;
            static void fun()
            {
                AcessModifiers am = new AcessModifiers();
            }
        }

        public static void Main(string[] args)
        {
            Console.WriteLine();
        }
    }
}

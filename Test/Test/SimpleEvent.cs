﻿using System;


namespace Test
{

    public class MyTest
    {
        private event EventHandler Ievent;
        public event EventHandler MyEvent
        {
            add
            {
                Ievent += value;
            }
            remove
            {
                Ievent -= value;
            }
        }
        public event EventHandler ImMyEvent;
        public void raise() {
            EventHandler e = ImMyEvent;
            e(this, null);
        }
    }
    public class Test
    {
        public void TestEvent()
        {
            MyTest myTest = new MyTest();
            myTest.MyEvent += myTest_MyEvent;
            myTest.MyEvent -= myTest_MyEvent;
            //myTest.raise();
            myTest.ImMyEvent += myTest_MyEvent;
            myTest.raise();
        }
        public void myTest_MyEvent(object sender, EventArgs e)
        {
            //Console.WriteLine("Hello");
        }
    }
    
    class SimpleEvent
    {
        static void Main(string[] args)
        {
            Test test = new Test();
            test.TestEvent();
            //Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace Test
{
    class Generic<T> where T:new()
    {
        public int a = 10;
        private Generic() { }
        public Generic(int a)
        {
           

        }
        public static void fun<T>(T obj) 
        {
            if (typeof(T) == typeof(int) || typeof(T)==typeof(string))
            {
                Console.WriteLine("Int");
            }
            else
            {
                Console.WriteLine("Nope :( "+ obj);
            }
            
        }
        
    }
    class TestAbc
    {
        public static void Main(string[] args)
        {
            //Generic<int> g = new Generic<int>();
            int a = 1;
            TestAbc t = new TestAbc();
            Generic<TestAbc>.fun(t);

            /*Console.WriteLine(g.a);
            List<int> data = new List<int>();
            //data.Add("1");
            data.Add(2);*/
            /*SortedList sl = new SortedList();
            SortedList<int,int> s2 = new SortedList<int,int>();
            s1.Add("1","1");
            s2.Add(1,1);*/
            Action<string> dl = getstring;
            Console.WriteLine(dl);
        }
        static void getstring(object t)
        {
        }
    }
}


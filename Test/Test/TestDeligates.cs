﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public delegate int returnTypeDelegate(int a,int b);
    public delegate void nonReturnTypeDelegate(int a,int b);
    class Cal
    {
        public static int Add(int a,int b)
        {
            return a + b;
        }
        public static int Mul(int a,int b)
        {
            return a * b;
        }
    }
    class Cal2
    {
        public static void Add(int a, int b)
        {
            Console.WriteLine("Cal2 Add called: {0}",a + b);
        }
        public static void Mul(int a, int b)
        {
            Console.WriteLine("Cal2 Mul called: {0}", a * b);
        }
    }
    class TestDeligates
    {
        public static void m1(int a,int b)
        {
            
        }
        public static void Main(string[] args)
        {
            int a = 0;
            Console.WriteLine(a);
            //Simple Delegate/singlecast
            returnTypeDelegate deli = new returnTypeDelegate(Cal.Add);
           Console.WriteLine("Multiple "+" "+deli(3,5));
            returnTypeDelegate deli2 = new returnTypeDelegate(Cal.Mul);
            //Console.WriteLine(deli2(4,7));

            //MultiCast Delegate
            nonReturnTypeDelegate NoReturnTypeDeli = new nonReturnTypeDelegate(Cal2.Add);
            //NoReturnTypeDeli += new nonReturnTypeDelegate(Cal2.Mul);//adding methods in this delegate
            NoReturnTypeDeli += new nonReturnTypeDelegate(Cal2.Mul)+ new nonReturnTypeDelegate(Cal2.Mul);//adding methods in this delegate
            
            NoReturnTypeDeli += m1;
            NoReturnTypeDeli(2, 10);//calls all the methods in this Delegate

            NoReturnTypeDeli -= new nonReturnTypeDelegate(Cal2.Add);//removing methods in this delegate
            NoReturnTypeDeli(2, 12);
            NoReturnTypeDeli= (nonReturnTypeDelegate)Delegate.Combine(NoReturnTypeDeli,NoReturnTypeDeli);
            foreach(var i in NoReturnTypeDeli.GetInvocationList())
            {
                Console.WriteLine(i.Method);
            }
            nonReturnTypeDelegate cloned = (nonReturnTypeDelegate)NoReturnTypeDeli.Clone();
            foreach(var i in cloned.GetInvocationList())
            {
                Console.WriteLine(i.Method);
            }
            //Lambda exp
            Func<int,int,int> Addition2=(a,b)=> a+b;
            //Console.WriteLine(Addition2(20,30));

            
            Action<int, int> Addition = addNumber;//oneway
            
            Addition(10, 20);
            //Console.WriteLine($"Addition={result}");

            Action<int, int> DemoAdd = new Action<int, int>(TestDeligates.addNumber);//2ndway
            //Action<int, int> DemoAdd = TestDeligates.addNumber;//3ndway
            DemoAdd(2,1);
            //Console.WriteLine(result);


            Predicate<string> Check = IsPerson;
            //Console.WriteLine(Check("person"));
        }
        public static bool IsPerson(string thing)
        {
            return thing == "person" ? true : false;
            
        }
        private static int result;
        public static void addNumber(int a,int b)
        {
            result=a+b;
        }
    }
}

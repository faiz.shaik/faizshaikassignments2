﻿using System;
namespace Test
{
    interface interface1
    {
        void call();
    }
    partial class A
    {
        partial void m1();
    }
    partial class A
    {
        partial void m1()
        {
            Console.WriteLine("hey");
        }
    }
    abstract class AbsClass:A,interface1
    {
        abstract public void call();
    }
    struct s1
    {

    }
    class B : AbsClass
    {
        public override void call()
        {

        }
    }
 
    class TypesOfClasses
    {
        public static void Main(string[] args)
        {

        }
    }
}

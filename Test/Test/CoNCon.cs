﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class Animal
    {
        public int speed ;
        void Eat() { Console.WriteLine("Eat"); }
    }
    class Dog : Animal
    {
        void DogFood() { Console.WriteLine("Dog Food"); }
    }
    class Cat : Animal
    {
        void CatFood() { Console.WriteLine("Cat Food"); }
    }
    delegate Animal ReturnAnimal();
    delegate Dog ReturnDog();
    
    interface ICovariant<out T>
    {
        T Name { get; }
        T Get();
    }
    interface IContravarient<in T>
    {
        T Name { set; }
        void Set(T t);
    }
    interface ICompination<in T,out U>
    {
        void Set(T item);
        U Get();
    }
    class CoNCon
    {
        public static Animal getAnimal() => new Animal();
        public static Dog getDog() => new Dog();
        public static void Main(string[] args)
        {


            Animal a = new Animal();
            Animal a2 = new Dog();
            Animal a3 = new Cat();

            //Dog d = new Animal();



            ICovariant<Animal> covarient = (ICovariant<Dog>)null;
            IContravarient<Dog> contravarient = (IContravarient<Animal>)null;

            var sortedSet = new SortedSet<Dog>(new AnimalCompare())
            {
                new Dog()
                {
                    speed=30
                },

                new Dog()
                {
                    speed =50
                },
                new Dog()
                {
                    speed=10
                }

            };
            foreach(var i in sortedSet)
            {
                Console.WriteLine("Speed of Dog: {0}",i.speed);
            }
        }
        class AnimalCompare : IComparer<Animal>
        {
            int IComparer<Animal>.Compare(Animal x, Animal y)
            {
                return x.speed - y.speed;
            }
        }
    }
}

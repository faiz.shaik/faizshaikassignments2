﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class T
    {
        public  void Run()
        {
            Console.WriteLine("Run");
        }
    }
    class ThreadExp
    {
        public void Display()
        {
            Console.WriteLine("Inside Display");
            
            //lock (this)
            //{
                Console.WriteLine("Inside Lock Statement");
            
                Console.WriteLine("Came Lock Statement");
            //}
            
        }
        
        static void Main(string[] args)
        {
            /*Console.WriteLine("Main Thread Started");
            ThreadExp exp = new ThreadExp();
            Thread t1 = new Thread(exp.Display);
            Thread t2 = new Thread(exp.Display);
            t1.Start(); t2.Start();
            t1.Join(); t2.Join();
            Console.WriteLine("Main Thread Exiting");*/
            T tree = new T();
            Thread nt = new Thread(new ThreadStart(tree.Run));
            nt.Start();
            if(!nt.Join(50))
                nt.Abort();
        }
    }
}

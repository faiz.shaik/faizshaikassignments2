﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class Indexer
    {
        int[] a=new int[2];
        string str;
        public int this[int val]
        {
            get
            {
                return a[val];
            }
            set
            {
                a[val]=value;
            }
        }
        public string this[string i]
        {
            get
            {
                return str;
            }
            set
            {
                str = value;
            }
        }
    }
    class InMain
    {
        public static void Main(string[] args)
        {
            Indexer id = new Indexer();
            id[0] = 12;
            id["0"] = "Sam";
            Console.WriteLine(id[0]+" "+id["0"]);
        }
    }
}

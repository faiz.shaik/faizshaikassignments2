﻿using System;
using System.Threading;

namespace Test
{
    class RecThreadWait
    {
        public static void SecondMethod()
        {
            Console.WriteLine("Second Method Called");
        }
        public static void Rec(Action method ,int time)
        {
            Console.WriteLine("Rec");
            method();
            Thread.Sleep(time);
            Rec(method,time);
            
        }
        public static void Main(string[] args)
        {
            //Rec(SecondMethod,2000);
            Rec(() => { Console.WriteLine("9"); },3000);
        }
    }
}

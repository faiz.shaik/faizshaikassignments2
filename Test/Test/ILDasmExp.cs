﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Demo
    {
        int a = 5;
        public Demo()
        {

        }
        public Demo(int b)
        {
            a = b;
        }
        public int Method2(int a,int b,int c)
        {
            return a + b + c;
        }
    }
    class ILDasmExp
    {
        public static void ILMethod(Demo d)
        {
            Console.WriteLine(d.Method2(1,2,3));
        }
        public static void Main(string[] args)
        {
            Demo d = new Demo();
            ILMethod(d);
        }
    }
}

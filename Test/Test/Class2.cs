﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class A1
    {
        protected int a = 10;
        protected void m1()
        {
            Console.WriteLine("A1-m1 called");
        }
    }
    class A2:A1
    {
        public void f()
        {
            a = 20;
            Console.WriteLine(a);
        }
        public void m2()
        {
            m1();
            Console.WriteLine("A2-m2 called");
        }
    }
    class A3:A2
    {
        public void f1()
        {
            a = 40;
            Console.WriteLine(a);
        }
    }
    class Class2
    {
        public static void Main(string[] args)
        {
            A1 a1 = new A1();
            A2 a2 = new A2();
            A3 a3 = new A3();
            a2.f();
            a3.f1();
        }
    }
}

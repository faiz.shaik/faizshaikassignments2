﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Collections.ObjectModel;

namespace Test
{
    class Collections
    {
        public static void Main(string[] args)
        {
            ArrayList al = new ArrayList(1);
            al.Add(1);
            al.Add(2);
            al.Add(3);
            Dictionary<string, string> dic = new Dictionary<string,string>();
            dic.Add("1", "2");
            //dic.Add("2","3");
            /*dic.Add("3", "4");
            dic["abc"] = "a";*/
            //Console.WriteLine(al.Count);
            Console.WriteLine(dic.Count);
            foreach(var i in dic)
            {
                Console.WriteLine(i);
            }
            List<int> list = new List<int>();
            list.Add(6);
            list.Add(2);
            list.Add(1);
            list.Add(4);
            list.Add(5);
            Console.WriteLine(list.BinarySearch(1));
            Console.WriteLine(list.Count+" "+list.Capacity);
            HashSet<string> set = new HashSet<string>();
            HashSet<string> set1 = new HashSet<string>();
            set.Add("A");
            set.Add("a");
            set1.Add("A");
            set1.ExceptWith(set);
            foreach(string i in set1)
                Console.WriteLine(i);
            ObservableCollection<int> p=new ObservableCollection<int>();
            Console.WriteLine(p);
        }
    }
}

﻿using System;

namespace Test
{
    class AnotherClass
    {
        public string name = "xyz";
        public void m1(Action f)
        {
            f();
        }
    }
    

    class Class3
    {
        public string name = "abc";
        public  void M12()
        {
            AnotherClass ac = new AnotherClass();
            ac.m1(() => Console.WriteLine(this.name));
        }
        public static void Main(string[] args)
        {
            Class3 cl3 = new Class3();
            cl3.M12();
        }
    }
}

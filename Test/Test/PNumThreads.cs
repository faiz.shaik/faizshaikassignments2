﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Test
{
    class PNumThreads
    {
        public static List<int> prime = new List<int>();
        public static int start1, start2,n;
        private static readonly object padlock = new object();
        public static bool IsPrime(int n)
        {
            for (int i = 2; i < n; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        public static void Method1()
        {
            int start = Thread.CurrentThread.Name == "Thread1" ? start1 : start2;
            int end = Thread.CurrentThread.Name == "Thread1" ? start2 : n;
            for (int i=start;i<end;i++)
            {

                if (IsPrime(i))
                {
                    Console.WriteLine(Thread.CurrentThread.Name);
                    lock (padlock)
                    {
                        Console.WriteLine(Thread.CurrentThread.Name);
                        if (!prime.Contains(i))
                        {
                            prime.Add(i);
                            //Console.WriteLine(Thread.CurrentThread.Name);
                            //Console.WriteLine(i);
                        }
                    }
                }
            }
        }
        public static void Call()
        {
            Thread t1 = new Thread(Method1);
            t1.Name = "Thread1";
            Thread t2 = new Thread(Method1);
            t2.Name = "Thread2";
            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();
        }
        public static void Main(string[] args)
        {
            
            while (true)
            {
                n = Convert.ToInt32(Console.ReadLine());
                if (prime.Count != 0)
                {
                    start1 = prime[prime.Count - 1];
                    int diff= n - (int)Math.Round(start1/10.0)*10;
                    start2 = (int)Math.Round(start1 / 10.0) * 10+diff /2;
                }
                else
                {
                    start1 = 2;
                    start2 = n / 2;
                }
                Call();
                prime.Sort();
                for (int i = 0; i < prime.Count; i++)
                {
                    Console.Write(prime[i] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
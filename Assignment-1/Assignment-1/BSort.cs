﻿using System;

namespace Assignment_1
{
    class BSort
    {
        static int[] Sort(int[] inputArray, int length)
        {
            int temp;
            int i;
            int j;
            for (i = 0; i < length; i++)
            {
                for (j = i + 1; j < length; j++)
                {
                    if (inputArray[i] > inputArray[j])
                    {
                        temp = inputArray[i];
                        inputArray[i] = inputArray[j];
                        inputArray[j] = temp;
                    }
                }
            }
            return inputArray;
        }
        static void Main(string[] args)
        {
            int length = Convert.ToInt32(Console.ReadLine());
            int[] inputArray = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            int i;
            inputArray = Sort(inputArray, length);
            for (i = 0; i < length; i++)
            {
                Console.Write(inputArray[i] + " ");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_1
{
    class QSort
    {
        static int length;
        static int[] input_array;
        static int Divide(int left,int right)
        {
            int pivot =input_array[right],pos=left;
            for(int i = left; i < right; i++)
            {
                if (input_array[i] < pivot)
                {
                    int temp1 = input_array[pos];
                    input_array[pos] = input_array[i];
                    input_array[i] = temp1;
                    pos++;
                }
            }
            int temp = input_array[pos];
            input_array[pos] = input_array[right];
            input_array[right] = temp;
            return pos;
        }
        static void Sort(int left,int right)
        {
            if (left >= right)
            {
                return;
            }
            int div =Divide(left,right);
            Sort(left, div - 1);
            Sort(div + 1, right);
        }
        static void Main(string[] args)
        {
            length = Convert.ToInt32(Console.ReadLine());
            input_array = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Sort(0, length - 1);
            for (int i = 0; i < length; i++)
            {
                Console.Write(input_array[i] + " ");
            }
        }
    }
}

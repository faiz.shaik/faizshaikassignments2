﻿using System;

namespace Assignment_1
{
    class LSearch
    {
        static int[] input_array;
        static int length, search;
        static int FindElement()
        {
            for(int i = 0; i < length; i++)
            {
                if (input_array[i] == search)
                {
                    return i;
                }
            }
            return -1;
        }
        static void Main(string[] args)
        {
            length= Convert.ToInt32(Console.ReadLine());
            input_array = new int[length];
            input_array=Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            search =Convert.ToInt32(Console.ReadLine());
            int result=FindElement();
            if (result==-1)
            {
                Console.WriteLine("Element " + search + " Not Found");
            }
            else
            {
                Console.WriteLine("Element Found at index "+result);
            }
        }
    }
}

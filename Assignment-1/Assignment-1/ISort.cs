﻿using System;

namespace Assignment_1
{
    class ISort
    {
        static int length;
        static int[] input_array;
        static void Sort()
        {
            for(int i = 1; i < length; i++)
            {
                int temp = input_array[i], j = i - 1;
                while (j>=0 && input_array[j] > temp)
                {
                    input_array[j + 1] = input_array[j];
                    j--;
                }
                input_array[j+1]=temp;
            }
        }
        static void Main(string[] args)
        {
            length = Convert.ToInt32(Console.ReadLine());
            input_array = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Sort();
            for(int i = 0; i < length; i++)
            {
                Console.Write(input_array[i]+" ");
            }
        }
    }
}

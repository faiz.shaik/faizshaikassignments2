﻿using System;

namespace Assignment_1
{
    class MSort
    {
        static int length;
        static int[] input_array;
        static void Merge(int left,int mid,int right)
        {
            int length1 = mid -left + 1;
            int length2 =right-mid;
            int[] left_array = new int[length1], right_array = new int[length2];
            for (int i1 = 0; i1 < length1; i1++)
                left_array[i1] = input_array[left+i1];
            for (int i1 = 0; i1 < length2; i1++)
                right_array[i1] = input_array[mid+1+i1];
            int i = 0, j = 0, k = left;
            while(i<length1 && j < length2)
            {
                if (left_array[i] < right_array[j])
                {
                    input_array[k] = left_array[i];
                    i++;
                }
                else
                {
                    input_array[k] = right_array[j];
                    j++;
                }
                k++;
            }
            while (i < length1)
            {
                input_array[k] = left_array[i];
                i++;
                k++;
            }
            while (j < length2)
            {
                input_array[k] = right_array[j];
                j++;
                k++;
            }
        }
        static void Sort(int left,int right)
        {
            if (left >= right)
            {
                return;
            }
            int mid = (left + right) / 2;
            Sort(left, mid);
            Sort(mid + 1, right);
            Merge(left, mid, right);
        }
        static void Main(string[] args)
        {
            length = Convert.ToInt32(Console.ReadLine());
            input_array = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Sort(0,length-1);
            for (int i = 0; i < length; i++)
            {
                Console.Write(input_array[i] + " ");
            }
        }
    }
}

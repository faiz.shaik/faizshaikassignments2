﻿using System;

namespace Assignment_1
{
    class RSort
    {
        public static int[] input_array;
        public static int length;
        public static int FindMax()
        {
            int max = input_array[0];
            foreach(int i in input_array)
            {
                max = max < i ? i : max;
            }
            return max;
        }
        public static void DigitSort(int digit)
        {
            int[] result_array = new int[length];
            int[] count = new int[10];
            foreach(int i in input_array)
            {
                count[(i / digit) % 10]++;
            }
            for(int i = 1; i < 10; i++)
            {
                count[i] += count[i - 1];
            }
            for (int i = length - 1; i >= 0; i--)
            {
                result_array[count[(input_array[i] / digit) % 10] - 1] = input_array[i];
                count[(input_array[i] / digit) % 10]--;
            }
            for (int i = 0; i < length; i++)
            {
                input_array[i] = result_array[i];
            }

        }
        public static void sort()
        {
            int max=FindMax();
            for(int i = 1; (max / i) > 0; i *= 10)
            {
                DigitSort(i);
            }

        }
        public static void Main(string[] args){
            length = Convert.ToInt32(Console.ReadLine());
            input_array = new int[length];
            input_array = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            sort();
            for(int i = 0; i < length; i++)
            {
                Console.Write(input_array[i]+" ");
            }
        }
    }
}

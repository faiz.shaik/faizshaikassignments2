﻿using System;

namespace Assignment_1
{
    class BSearch
    {
        static int FindElement(int[] inputArray, int search, int left, int right)
        {
            int mid = (left + right) / 2;
            if (left > right)
            {
                return -1;
            }
            if (inputArray[mid] == search)
            {
                return mid;
            }
            else if (inputArray[mid] < search)
            {
                return FindElement(inputArray, search, mid + 1, right);
            }
            return FindElement(inputArray, search, left, mid - 1);
        }
        static void Main(string[] args)
        {
            int length = Convert.ToInt32(Console.ReadLine());
            int[] inputArray = new int[length];
            inputArray = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            int search = Convert.ToInt32(Console.ReadLine());
            int result = FindElement(inputArray, search, 0, length);
            if (result == -1)
            {
                Console.WriteLine("Element " + search + " Not Found");
            }
            else
            {
                Console.WriteLine("Element Found at index " + result);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_1
{
    class HSort
    {
        public static int[] input_array;
        public static int length;
        public static void sort()
        {
            for (int i = (length / 2) - 1; i >= 0; i--)
            {
                heapify(length,i);
            }
            for(int i = length - 1; i >= 0; i--)
            {
                int temp = input_array[0];
                input_array[0] = input_array[i];
                input_array[i] = temp;
                heapify(i,0);
            }
        }
        public static void heapify(int new_length,int index)
        {
            int parent = index, left = 2 * index + 1, right = 2 * index + 2;
            if(left<new_length && right<new_length && (input_array[left] > input_array[parent] || input_array[right]>input_array[parent]))
            {
                parent = input_array[right] > input_array[left] ? right : left;
            }
            if (parent != index)
            {
                int temp = input_array[index];
                input_array[index] = input_array[parent];
                input_array[parent] = temp;
                heapify(new_length,parent);
            }
        }
        public static void Main(string[] args)
        {
            length = Convert.ToInt32(Console.ReadLine());
            input_array = new int[length];
            input_array = Array.ConvertAll(Console.ReadLine().Split(" "),int.Parse);
            sort();
            for(int i = 0; i < length; i++)
            {
                Console.Write(input_array[i]+" ");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using System.Linq;

namespace Assignment_2
{
    class Students
    {
        public static Dictionary<string, int> count = new Dictionary<string, int>();
        public static Dictionary<string, string> courseid = new Dictionary<string, string>();
        public static Dictionary<string, string> courseidrev = new Dictionary<string, string>();
        public static int totalcount;
        public static string filepath = Program.filepath2;
        public static List<SearchParameters> searchParameters = Program.studentsearchParameters;

        public void csvUtility(List<SearchParameters> courseTable)
        {

            totalcount = 0;
            foreach (var i in Program.searchParameters)
            {
                if (!courseid.ContainsKey(i.Name))
                {
                    courseid.Add(i.Name, i.ID);

                }
            }
            foreach (var i in Program.searchParameters)
            {
                if (!courseidrev.ContainsKey(i.ID))
                {
                    courseidrev.Add(i.ID, i.Name);
                }
            }

            foreach (var i in searchParameters)
            {
                if (i.Course.Contains("&"))
                {
                    string[] divcourses = i.Course.Split("&");
                    foreach (string incourse in divcourses)
                    {
                        if (!count.ContainsKey(courseidrev[incourse]))
                        {
                            count.Add(courseidrev[incourse], 1);
                        }
                        else
                        {
                            count[courseidrev[incourse]] += 1;
                        }
                    }
                }
                else
                {
                    if (!count.ContainsKey(courseidrev[i.Course]))
                    {
                        count.Add(courseidrev[i.Course], 1);
                    }
                    else
                    {
                        count[courseidrev[i.Course]] += 1;
                    }
                }
                totalcount++;
            }
            foreach (var i in courseTable)
            {
                if (!count.ContainsKey(i.Name))
                {
                    count.Add(i.Name, 0);
                }
            }
        }

        public void ViewStudents()
        {
            Console.WriteLine("All the Student who are registered into the courses:-");
            foreach (var i in searchParameters)
            {
                Console.WriteLine(i.FirstName + " " + i.LastName);
            }
        }

        public void AvailableCourse(List<SearchParameters> courseTable)
        {
            List<string> menu = new List<string>();
            int k = 1;
            foreach (var i in courseTable)
            {
                if (count[i.Name] < int.Parse(i.TotalSeats))
                {
                    menu.Add(i.Name);
                    Console.WriteLine(k + " " + i.Name);
                    k++;
                }
            }
            Console.WriteLine(k + ".Go Back To Main Menu\n"
                + (k + 1) + ".Exit\n");
            menu.Add("Go Back To Main Menu");
            menu.Add("Exit");
            string new_option2 = Console.ReadLine();
            if (menu[int.Parse(new_option2) - 1].Equals("Go Back To Main Menu"))
            {
                return;
            }
            else if (menu[int.Parse(new_option2) - 1].Equals("Exit"))
            {
                Environment.Exit(0);
            }
            else
            {
                Option1.Option1_1(menu[int.Parse(new_option2) - 1]);
            }
        }
        public static Dictionary<string, int> TSeats = new Dictionary<string, int>();
        public static string studentcourse = "";
        public static Boolean courseCheck()
        {
            string[] selectcourse = new string[TSeats.Count];
            if (cflag == 0)
            {
                Console.WriteLine("Select Course:");
                int k = 0;
                foreach (string i in TSeats.Keys)
                {
                    Console.WriteLine((k + 1) + " " + i);
                    selectcourse[k] = i;
                    k++;
                }
            }
            string pos = "";
            if (cflag == 1)
            {
                int k = 0;
                foreach (string i in TSeats.Keys)
                {
                    selectcourse[k] = i;
                    k++;
                }
                for (int i = 0; i < selectcourse.Length; i++)
                {
                    if (selectcourse[i].ToLower().Equals(Option1.scourse.ToLower()))
                    {
                        pos = i.ToString();
                    }
                }
            }
            string course = cflag == 0 ? Console.ReadLine() : pos;
            if (int.Parse(course) > 0 && int.Parse(course) <= TSeats.Count)
            {
                course = selectcourse[int.Parse(course) - 1];
                if (TSeats[course] - count[course] == 0)
                {
                    Console.WriteLine("Sorry the " + course + " Course is Full");
                    Console.WriteLine("If you wanted to Change course press 1 else, if you wanted to exit press 2");
                    string flag = Console.ReadLine();
                    if (flag[0] == '1')
                    {
                        courseCheck();
                    }
                    else
                    {
                        return false;
                    }
                }
                studentcourse = course;
                return true;
            }
            else
            {
                Console.WriteLine("Invalid input, Please Try again");
                return false;
            }
        }
        public static int cflag = 0;
        public static void RegisterStudent(List<SearchParameters> courseTable)
        {
            foreach (var i in courseTable)
            {
                if (!TSeats.ContainsKey(i.Name))
                {
                    TSeats.Add(i.Name, int.Parse(i.TotalSeats));
                }
            }
            Console.WriteLine("\nRegister New Student:");
            Boolean check = courseCheck();
            if (!check)
            {
                return;
            }
            Console.WriteLine("Student ID:");
            string sid = Console.ReadLine();
            Console.WriteLine("FirstName:");
            string fname = Console.ReadLine();
            Console.WriteLine("LastName:");
            string lname = Console.ReadLine();
            Console.WriteLine("Email:");
            string email = Console.ReadLine();
            check = EmailCheck(email);
            if (!check)
            {
                Console.WriteLine("Invalid Email");
                return;
            }
            Console.WriteLine("Phone Number:");
            string pnumber = Console.ReadLine();
            if (pnumber.Length == 10)
            {
                pnumber = "+91" + pnumber;
            }
            check = PhoneCheck(pnumber);
            if (!check)
            {
                Console.WriteLine("Invalid Phone Number");
                return;
            }
            check = Existing(fname, lname, email, pnumber, courseid[studentcourse]);
            if (check)
            {
                Console.WriteLine("Student already Registered");
                return;
            }
            if (nflag != 2)
            {
                searchParameters.Add(new SearchParameters
                {
                    ID = sid,
                    FirstName = fname,
                    LastName = lname,
                    Email = email,
                    Mobile = pnumber,
                    Course = courseid[studentcourse]
                });
                using (FileStream fs = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(sid + "," + fname + "," + lname + "," + email + "," + pnumber + "," + courseid[studentcourse]);
                    }
                }
            }
            else
            {
                File.WriteAllText(filepath, string.Empty);
                using (FileStream fs = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("ID,FirstName,LastName,Email,Mobile,Course");
                        foreach (var i in searchParameters)
                        {
                            sw.WriteLine(i.ID + "," + i.FirstName + "," + i.LastName + "," + i.Email + "," + i.Mobile + "," + i.Course);
                        }
                    }
                }
            }
            TSeats[studentcourse]++;
            count[studentcourse]++;
        }
        public static int nflag = 0;
        public static Boolean Existing(string fname, string lname, string email, string pnumber, string course)
        {
            nflag = 0;
            foreach (var i in searchParameters)
            {
                if ((i.FirstName + i.LastName).ToLower().Equals((fname + lname).ToLower()))
                {
                    nflag = 1;
                    if (i.Mobile.Equals(pnumber) && i.Email.Equals(email))
                    {
                        nflag = 2;
                        if (i.Course.Equals(course))
                        {
                            nflag = 3;
                        }
                        i.Course += "&" + course;
                    }
                }

            }
            if (nflag == 3)
                return true;
            return false;
        }
        public static Boolean EmailCheck(string email)
        {
            int flag = 0;
            for (int i = 0; i < email.Length; i++)
            {
                if (email[i] == '@' && i != 0)
                {
                    flag = 1;
                }
                if (flag == 1 && email[i] == '.' && email[i - 1] != '@' && i != email.Length - 1)
                {
                    return true;
                }
            }
            return false;
        }
        public static Boolean PhoneCheck(string pnumber)
        {
            string tpnumber = pnumber.Substring(1);
            if (pnumber[0] == '+' && pnumber.Length - 1 == 12 && tpnumber.All(char.IsDigit))
            {
                return true;
            }
            return false;
        }

    }
}

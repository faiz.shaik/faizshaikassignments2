﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using LumenWorks.Framework.IO.Csv;

namespace Assignment_2
{
    class Program
    {
        public static string option,filepath,filepath2;
        public static List<SearchParameters> searchParameters= new List<SearchParameters>();
        public static List<SearchParameters> studentsearchParameters= new List<SearchParameters>();
        public static void Input()
        {
            Console.WriteLine("\nPlease Choose of the Following Options:");
            Console.WriteLine("1.View all Courses\n" +
                "2.View only Available Courses\n"
                + "3.Add New Course\n"
                + "4.View all Students\n"
                + "5.New Student Registration\n"
                + "9.Exit\n");
            option = Console.ReadLine();
            if (option.Equals("9"))
            {
                return;
            }
            
            CheckInput();
            Input();
        }
        public static void CheckInput()
        {
            if (option.Length == 1 && option[0] > '0' && option[0] <= '5')
            {
                Students students = new Students();
                
                
                students.csvUtility(searchParameters);
                if (option[0] == '1')
                {
                    Option1.Start();
                }

                else if (option[0] == '2')
                {
                    Console.WriteLine("\nAvailable Courses are:");
                    students.AvailableCourse(searchParameters);
                }

                else if (option[0] == '3')
                {
                    Console.WriteLine("\nAdd New Courses:");
                    Console.WriteLine("Course ID:");
                    string id = Console.ReadLine();
                    Console.WriteLine("Course Name:");
                    string name = Console.ReadLine();
                    if (checkCourse(name, searchParameters))
                    {
                        Console.WriteLine("Course Already Exists!");
                    }
                    else
                    {
                        Console.WriteLine("Course Description:");
                        string description = Console.ReadLine();
                        if (description.Contains(","))
                        {
                            string update = "";
                            for (int i = 0; i < description.Length; i++)
                            {
                                if (description[i] != ',')
                                {
                                    update += description[i];
                                }
                                else
                                {
                                    update += " ";
                                }
                            }
                        }
                        Boolean flag = true;
                        string seats = "";
                        while (flag)
                        {
                            Console.WriteLine("Course TotalSeats:");
                            seats = Console.ReadLine();
                            if (!seats.All(char.IsDigit))
                            {
                                Console.WriteLine("Total Seats must be in numerical format, Please try again");
                            }
                            else
                            {
                                flag = false;
                            }
                        }
                        searchParameters.Add(new SearchParameters
                        {
                            ID = id,
                            Name = name,
                            Description = description,
                            TotalSeats = seats
                        });
                        using (FileStream fs = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                        {
                            using (StreamWriter sw = new StreamWriter(fs))
                            {
                                sw.WriteLine(id + "," + name + "," + description + "," + seats);
                            }
                        }
                    }
                }
                if (option[0] == '4')
                {
                    students.ViewStudents();
                }

                if (option[0] == '5')
                {
                    Students.RegisterStudent(searchParameters);
                }
            }
            else
            {
                Console.WriteLine("\nInvalid Input Please try again");
                Input();
            }
        }
        public static Boolean checkCourse(string name,List<SearchParameters> searchParameters)
        {
            Boolean flag = false;
            foreach(var i in searchParameters)
            {
                if(name.Replace(" ","").ToLower().Equals(i.Name.Replace(" ", "").ToLower()))
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome User!");
            filepath = @"C:\CSVFiles\Model-CoursesTable.csv";
            var csvTable = new DataTable();
            using (var csvReader = new CsvReader(new StreamReader(File.OpenRead(filepath)), true))
            {
                csvTable.Load(csvReader);
            }
            for (int i = 0; i < csvTable.Rows.Count; i++)
            {
                searchParameters.Add(new SearchParameters
                {
                    ID = csvTable.Rows[i][0].ToString(),
                    Name = csvTable.Rows[i][1].ToString(),
                    Description = csvTable.Rows[i][2].ToString(),
                    TotalSeats = csvTable.Rows[i][3].ToString()
                });
            }

            filepath2 = @"C:\CSVFiles\Model-StudentsTable.csv";
            var studentTable = new DataTable();
            using (var csvReader = new CsvReader(new StreamReader(File.OpenRead(filepath2)), true))
            {
                studentTable.Load(csvReader);
            }

            for (int i = 0; i < studentTable.Rows.Count; i++)
            {
                studentsearchParameters.Add(new SearchParameters
                {
                    ID = studentTable.Rows[i][0].ToString(),
                    FirstName = studentTable.Rows[i][1].ToString(),
                    LastName = studentTable.Rows[i][2].ToString(),
                    Email = studentTable.Rows[i][3].ToString(),
                    Mobile = studentTable.Rows[i][4].ToString(),
                    Course = studentTable.Rows[i][5].ToString(),
                });
            }

            Input();
        }
    }
}

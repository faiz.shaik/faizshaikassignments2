﻿using System;
using System.Collections.Generic;

namespace Assignment_2
{
    class Option1
    {
        public static List<string> Menu=new List<string>();
        public static void Start()
        {
            Console.WriteLine("\nAll The Courses are:");
            int k = 0;
            foreach (var i in Program.searchParameters)
            {
                Console.WriteLine(k+1+" "+i.Name);
                if (!Menu.Contains(i.Name))
                {
                    Menu.Add(i.Name);
                }
                k++;
            }
            Console.WriteLine(k+1+".Go Back To Main Menu\n"
                + (k+2)+".Exit\n");
            Menu.Add("Go Back To Main Menu");
            Menu.Add("Exit");
            string new_option1 = Console.ReadLine();
            if (Menu[int.Parse(new_option1)-1].Equals("Go Back To Main Menu"))
            {
                return;
            }
            else if (Menu[int.Parse(new_option1)-1].Equals("Exit"))
            {
                Environment.Exit(0);
            }
            else
            {
                Option1_1(Menu[int.Parse(new_option1)-1]);
            }
        }
        public static string scourse;
        public static void Option1_1(string selected_course)
        {
            Console.WriteLine(selected_course
                +"\n1.View all Student in this course\n"
                +"2.Register New Student in this course\n"
                +"3.Go Back To Previous Menu\n"
                +"4.Go Back To Main Menu\n"
                +"5.Exit\n");
            string option1_1 = Console.ReadLine();
            if (option1_1.Equals("4"))
            {
                Program.Input();
            }
            else if (option1_1.Equals("3"))
            {
                Start();
            }
            else if (option1_1.Equals("5"))
            {
                Environment.Exit(0);
            }
            else if(option1_1.Equals("1"))
            {
                Console.WriteLine("All the Student who are registered into {0}:-",selected_course);
                foreach (var i in Students.searchParameters)
                {
                    if (i.Course.Contains(Students.courseid[selected_course])) {
                        Console.WriteLine(i.FirstName + " " + i.LastName);
                    }
                }
            }
            else if (option1_1.Equals("2"))
            {
                Students.cflag = 1;
                scourse = selected_course;
                Students.RegisterStudent(Program.searchParameters);
            }
            else
            {
                Console.WriteLine("Invalid input! please try again");
                Option1_1(selected_course);
            }
        }
    }
}

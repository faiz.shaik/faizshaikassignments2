﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_2
{
    class SearchParameters
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TotalSeats { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile{ get; set; }
        public string Email{ get; set; }
        public string Course{ get; set; }
    }
}
